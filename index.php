<?php
/**
 * @file index.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:33
 * @brief 网站的唯一入口文件
 */

define('CHAN_CMS', true);
define('DEBUG', true);

require './source/init.php';

App::run();