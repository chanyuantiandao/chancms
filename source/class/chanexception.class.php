<?php
/**
 * @file chanexception.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:03
 * @brief 网站的异常类
 */

!defined('CHAN_CMS') && exit('非法访问！');
class ChanException extends Exception
{

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('ChanException: '.$message, $code, $previous);
    }
}