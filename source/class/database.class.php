<?php
/**
 * @file database.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:03
 * @brief 扩展数据库的接口
 */

!defined('CHAN_CMS') && exit('非法访问！');

interface Database
{
    /**
     * @param $config array 数据库配置信息，具体配置信息参见common.config.php
     * @return mixed 数据库对象实例
     */
    public static function getInstance(array $config) : Database;

    /**
     * @param $table string 数据表表名，不含表前缀
     * @return mixed 以数组形式返回对应数据表的结构信息
     */
    public function getFieldsInfoOfTable(string $table) : array;


    /**
     * @brief 开启一个事务，如果数据库不支持事务则忽略
     * @param bool $flag 如果数据库不支持事务的话，是否继续执行
     * @return bool 启动事务是否成功
     */
    public function beginTransaction(bool $flag = true) : bool;

    public function commit() : bool;

    public function rollback() : bool;

    /**
     * @brief 数据库插入数据
     * @param string $table 要操作的数据表名
     * @param array $data 要插入的数据信息，字段作为key
     * @param bool $isReplace 如果存在重复数据是否使用replace代替insert
     * @return bool 返回Boolean值：true或者false
     */
    public function insert(string $table, array $data, bool $isReplace = false) : bool;

    public function getLastInsertId() : int;

    public function select(string $table, string $fields, array $where, array $orderBy, array $limit) : bool;

    public function update(string $table, array $data, array $where) : bool;

    public function delete(string $table, array $where) : bool;

    public function fetchData() : array;

    public function getAffectedRow() : int;

    /**
     * @brief 当数据库操作失败时，返回错误信息
     * @return array 返回数组的形式为array('errorCode'=>0, 'errorInfo'=>'');
     */
    public function getLastError() : array;

    public function getLastSql() : string;

}
