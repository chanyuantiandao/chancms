<?php
/**
 * @file FileHelper.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:54
 * @brief TODO
 */

!defined('CHAN_CMS') && exit('非法访问！');

class FileHelper
{
    /**
     * @brief 使用DIRECTORY_SEPARATOR拼接路径
     * @param ...$folders string 要拼接的文件夹（不可以是文件）
     * @return string 拼接完的路径
     */
    public static function joinPathWithRoot(...$folders){
        $path = ROOT_DIRECTORY;
        foreach ($folders as $folder){
            $folder = trim($folder, '/\\');
            $path .= ($folder.DIRECTORY_SEPARATOR);
        }
        return $path;
    }

    public static function joinPath(...$folders){
        $path = '';
        foreach ($folders as $folder){
            $folder = trim($folder, '/\\');
            $path .= ($folder.DIRECTORY_SEPARATOR);
        }
        return $path;
    }

}