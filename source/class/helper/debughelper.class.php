<?php
/**
 * @file debughelper.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-18 09:37
 * @brief TODO
 */

!defined('CHAN_CMS') && exit('非法访问！');

define('DEBUG_INFO', 0);
define('DEBUG_ERROR', 1);

class DebugHelper
{
    private static $debugInfo = array();

    public static function show($message){
        if (DEBUG){
            echo $message;
        }
    }

    public static function dump($arrMessage){
        if(DEBUG){
            echo json_encode($arrMessage);
        }
    }

    public static function log($file, $line, $level, $message, $data = array()){
        if(DEBUG){
            array_push(self::$debugInfo, array($file, $line, $level, $message, $data));
        }
    }

    public static function getInfo(){
        return DEBUG ? self::$debugInfo : array();
    }
}