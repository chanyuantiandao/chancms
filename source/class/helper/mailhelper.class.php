<?php
/**
 * @file mailhelper.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-28 15:30
 * @brief TODO
 */
!defined('CHAN_CMS') && exit('非法访问！');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// 引入PHPMailer的核心文件
$currentPath = FileHelper::joinPathWithRoot('source', 'class', 'thirdparty', 'PHPMailer-6.5.3', 'src');
require_once($currentPath.'PHPMailer.php');
require_once($currentPath.'SMTP.php');

class MailHelper
{

    /**
     * @param array $recipients 邮件接收者的地址数组
     * @param string $subject 要发送邮件的标题
     * @param string $body 要发送邮件的正文部分，支持HTML
     * @param array $attachments 邮件附件的地址数组，可以为空
     * @return bool 邮件是否发送成功
     * @throws ChanException 当存在以下情况是会抛出ChanException异常：① 未配置smtp服务器信息； ② 提供了错误格式的邮箱地址； ③ 附件路径不正确 ④ PHPMailer内部发生错误时候。
     */
    public static function send(array $recipients, string $subject, string $body, array $attachments = array()){
        $config = App::config('mail', null);
        if($config == null){
            throw new ChanException('尚未配置smtp服务器相关信息！');
        }
        // 实例化PHPMailer核心类
        $mailer = new PHPMailer(true);
        try {
            // 是否启用smtp的debug进行调试 开发环境建议开启 生产环境注释掉即可 默认关闭debug调试模式
            $mailer->SMTPDebug = 0;
            // 使用smtp鉴权方式发送邮件
            $mailer->isSMTP();
            // smtp需要鉴权 这个必须是true
            $mailer->SMTPAuth = true;
            // 链接qq域名邮箱的服务器地址
            $mailer->Host = $config['smtp'];
            // 设置使用ssl加密方式登录鉴权
            $mailer->SMTPSecure = 'ssl';
            // 设置ssl连接smtp服务器的远程服务器端口号
            $mailer->Port = $config['port'];
            // 设置发送的邮件的编码
            $mailer->CharSet = $config['charset'];
            // 设置发件人昵称 显示在收件人邮件的发件人邮箱地址前的发件人姓名
            $mailer->FromName = $config['nickname'];
            // smtp登录的账号 QQ邮箱即可
            $mailer->Username = $config['username'];
            // smtp登录的密码 使用生成的授权码
            $mailer->Password = $config['password'];
            // 设置发件人邮箱地址 同登录账号
            $mailer->From = $config['username'];
            // 邮件正文是否为html编码 注意此处是一个方法
            $mailer->isHTML(true);
            // 设置收件人邮箱地址
            // 添加多个收件人 则多次调用方法即可
            foreach ($recipients as $recipient){
                if(!isEmail($recipient)){
                    throw new ChanException('使用了错误格式的邮箱地址：'.$recipient.'！');
                }
                $mailer->addAddress($recipient);
            }
            // 添加该邮件的主题
            $mailer->Subject = $subject;
            // 添加邮件正文
            $mailer->Body = $body;
            // 为该邮件添加附件
            foreach ($attachments as $attachment){
                if(!file_exists($attachment)){
                    throw new ChanException('找不到附件，使用了错误的附件地址：'.$attachment.'！');
                }
                $mailer->addAttachment($attachment);
            }
            // 发送邮件 返回状态
            return  $mailer->send();
        }catch(Exception $e){
            throw new ChanException('邮件发送失败，错误信息：'.$mailer->ErrorInfo.'！');
        }

    }

}