<?php
/**
 * @file common.config.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 14:55
 * @brief 通用信息配置文件
 */

!defined('CHAN_CMS') && exit('非法访问！');

$config = array(

    'default' => array(
        'mod' => 'index',   // Controller
        'act' => 'index',   // action
        'cp' => 1,           // currentpage
    ),

    'rewrite' => array(
        'enable' => false
    ),

    'database' => array(
        'host' => '127.0.0.1',
        'port' => 3306,
        'type' => 'mysql',
        'uname' => 'root',
        'upass' => '123456',
        'dbname' => 'caedoc',
        'table_pre' => 'cae_',
        'charset' => 'utf8',
        'cache' => true,
        'cache_expire_time' => 3600  //单位秒
    ),

    'mail' => array(
        'smtp' => 'smtp.qq.com',     // 发送邮件服务器地址
        'port' => 465,               // 设置ssl连接smtp服务器的远程服务器端口号
        'charset' => 'UTF-8',        // 设置发送的邮件的编码
        'nickname' => '佛系学生',     // 设置发件人昵称 显示在收件人邮件的发件人邮箱地址前的发件人姓名
        'username' => '',           // smtp登录的账号 即邮箱地址，例：chanyuantiandao@126.com
        'password' => ''            // smtp登录的密码 一般使用生成的授权码
    ),

    'cache' => array(
        'enable' => true,      //暂时不支持
        'level' => array('sql', 'php', 'html'),
        'expire_time' => 3600,
        'path' => 'data/cache/'
    ),

    'log' => array(
        'enable' => false,      //暂时不支持
        'path' => 'data/logs/',
        'size' => 3*1024*1024, //3M
    ),

    'upload' => array(
        'allowable' => array('.jpg', '.jpeg', '.png', '.gif', '.bmp'),
        'maxsize' => 3*1024*1024, //3M
        'path' => 'data/upload/'
    ),

    'template' => array(
        'path' => 'source/view/',
        'theme' => 'default',
        'suffix' => '.tpl',
        'start_tag' => '{',
        'end_tag' => '}'
    )

);