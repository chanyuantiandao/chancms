<?php
/**
 * @file indexcontroller.class.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:33
 * @brief IndexController类
 */

!defined('CHAN_CMS') && exit('非法访问！');

class IndexController extends Controller
{
    function index(){
        $data = array(
            //'id' => '1523',
            'name' => '智慧',
            'password'=>''
        );
        $result = M('users')->data($data)->add();
        if($result === false){
            echo '保存失败！';
        }else{
            echo '插入成功，ID = '.$result;
        }


        $result = M('users')->data(array('name'=>'zh'))->where(array(array('id','1640','=')))->update();
        if($result){
            echo '更新成功！';
        }else{
            echo '更新失败！';
        }

        $result = M('users')->where(array(array('id','1640','<')))->remove();
        if($result){
            echo '删除成功！';
        }else{
            echo '删除失败！';
        }

        $result = M('users')->where(array(array('name','AX','=')))->limit(2,2)->order(array('id'=>'Desc'))->fetch();
        if($result){
            echo '查找成功！';
            $this->assign('result', $result);
        }else{
            echo '查找失败！';
        }





        $app = new stdClass;
        $app->name = "fal";
        $this->assign('app', $app);
        $this->assign('data', '测试数据');
        $this->assign('items', array('app', 'exe', 'json'));
        $this->display();
    }
}