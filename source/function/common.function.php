<?php
/**
 * @file common.function.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:33
 * @brief 通用函数定义文件
 */

!defined('CHAN_CMS') && exit('非法访问！');
define('CHANCMS_COMMON_FUNCTION', true);

function getPositiveIntVal($value, $default=null){
    if(!empty($value) && preg_match('/^[1-9][0-9]*$/', $value)){
        return intval($value);
    }else{
        return $default;
    }
}


function getIntVal($value, $default=null){
    if(!empty($value) && preg_match('/^[0-9]+$/', $value)){
        return intval($value);
    }else{
        return $default;
    }
}

function getBooleanVal($value, $default = null){
    if($value === 'true' || $value === 'True' || $value === 'TRUE' || $value === true){
        return true;
    }else if($value === 'false' || $value === 'False' || $value === 'FALSE' || $value === false){
        return false;
    }else{
        return $default;
    }
}


function getPureString($value, $default = null){
    if(!empty($value) && preg_match('/^[\w\d]+$/', $value)){
        return $value;
    }else{
        return $default;
    }
}


function getNumber($value, $default = null){
    if(!empty($value) && is_numeric($value)){
        return floatval($value);
    }else{
        return $default;
    }
}

function getSaftyText($value, $default = null){
    if(!empty($value)){
        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
    }else{
        return $default;
    }
}

function getMd5($value, $length = 0){
    if($length <= 0){
        return md5($value);
    }else{
        return substr(md5($value), 0, $length);
    }
}

function M($table){
    return new Model($table);
}

function getClientIP()
{
    static $realip;
    if (isset($_SERVER)){
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {
            $realip = $_SERVER["HTTP_CLIENT_IP"];
        } else {
            $realip = $_SERVER["REMOTE_ADDR"];
        }
    } else {
        if (getenv("HTTP_X_FORWARDED_FOR")){
            $realip = getenv("HTTP_X_FORWARDED_FOR");
        } else if (getenv("HTTP_CLIENT_IP")) {
            $realip = getenv("HTTP_CLIENT_IP");
        } else {
            $realip = getenv("REMOTE_ADDR");
        }
    }
    return $realip;
}

function checkrobot($useragent=''){
    static $kw_spiders = array('bot', 'crawl', 'spider' ,'slurp', 'sohu-search', 'lycos', 'robozilla');
    static $kw_browsers = array('msie', 'netscape', 'opera', 'konqueror', 'mozilla');

    $useragent = strtolower(empty($useragent) ? $_SERVER['HTTP_USER_AGENT'] : $useragent);
    if(strpos($useragent, 'http://') === false && in_array($useragent, $kw_browsers)) return false;
    if(in_array($useragent, $kw_spiders)) return true;
    return false;
}

function compressCode(string $content, string $lang = '') {
    $content = str_replace('\r\n', '', $content); //清除换行符
    $content = str_replace('\n', '', $content); //清除换行符
    $content = str_replace('\t', '', $content); //清除制表符
    $patterns = array(
        '/[\s]+/' => ' ',
        '/" /' => '"',
        '/ "/' => '"',
    );
    $php = array(

    );
    $html = array(
        '/> *([^ ]*) *</' => '>\\1<', //去掉注释标记
        '/<!--[^!]*-->/' => '',
        '\'/\*[^*]*\*/\'' => ''
    );
    switch ($lang){
        case 'html':
            $patterns = array_merge($patterns, $html);
            break;
        case 'php':
            $patterns = array_merge($patterns, $php);
            break;
        default:
            break;
    }
    return preg_replace(array_keys($patterns), array_values($patterns), $content);
}

function isEmail(string $email){
    return preg_match('/^[a-z\d]+(\.[a-z\d]+)*@([\da-z](-[\da-z])?)+(\.{1,2}[a-z]+)+$/', $email);
}