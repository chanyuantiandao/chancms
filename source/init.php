<?php
/**
 * @file init.php
 * @author 禅元天道 chanyuantiandao@126.com
 * @DateTime 2022-01-17 15:33
 * @brief 网站初始化文件
 */

!defined('CHAN_CMS') && exit('Illegal Access!');
!defined('DEBUG') && define('DEBUG', false);

if(version_compare(PHP_VERSION, '7.0.0') < 0){
    exit("请在PHP7.0.0以上的版本中运行此程序！当前PHP版本：" . PHP_VERSION);
}

if (isset($_GET['GLOBALS']) ||isset($_POST['GLOBALS']) ||  isset($_COOKIE['GLOBALS']) || isset($_FILES['GLOBALS'])) {
    exit('非法请求！');
}

define('ROOT_DIRECTORY', substr(dirname(__FILE__), 0, -6));

/// 页面的开始时间
define('PAGE_START_TIME', microtime(true));

/// 类的自动加载
function autoload($object){
    $systemPath = ROOT_DIRECTORY.'source/class/';
    $object_class_file = strtolower($object).'.class.php';
    if(file_exists($systemPath.$object_class_file)){
        require_once $systemPath.$object_class_file;
    }else if(!empty(App::config('autoload'))){
        foreach (App::config('autoload') as $path){
            if(file_exists(ROOT_DIRECTORY.$path.$object_class_file)){
                require_once ROOT_DIRECTORY.$path.$object_class_file;
                break;
            }
        }
    }else{
        throw new ChanException('找不到'.$object.'类文件！');
    }
}
spl_autoload_register('autoload');

if(DEBUG){
    error_reporting(E_ALL);
}else{
    error_reporting(0);

    // 设置用户自定义的错误处理函数
    $funcName = set_error_handler(array('App', 'error_handler'));
    // 注册一个会在php中止时执行的函数
    register_shutdown_function(array('App', 'shutdown_handler'));
}

/// 设置用户自定义的异常处理函数
set_exception_handler(array('App', 'exception_handler'));


if(!defined('CHANCMS_COMMON_FUNCTION') && !include(ROOT_DIRECTORY.'source/function/common.function.php')){
    throw new ChanException('读取公共函数文件common.function.php失败！');
}
if(!defined('CHANCMS_ENCRYPT_FUNCTION') && !include(ROOT_DIRECTORY.'source/function/encrypt.function.php')){
    throw new ChanException('读取加密函数文件encrypt.function.php失败！');
}
